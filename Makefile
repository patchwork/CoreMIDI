all:coremidi-cffi.lisp macerrors.lisp


headers.e:headers.h
	$(CC) -E headers.h -o headers.e


headers.c:headers.e Makefile
	sed -n -e 's-^ *__int32_t  *fds_bits\[\(.*\)\].*$$-#include <stdio.h>@#include <CoreMIDI/CoreMIDI.h>@int main(void){ printf("%lu %d\\n",\1,kMIDIThruConnection_MaxEndpoints); return 0;}-p' < headers.e | tr '@' '\012' > size.c
	$(CC) -o size size.c
	./size | ( read size kMIDIThruConnection_MaxEndpoints; sed \
	-e 's/typedef __darwin_wchar_t wchar_t;//' \
	-e 's/^ *:1,/ __filler1 :1,/' \
	-e 's/^ *:2,/ __filler2 :2,/' \
	-e 's/^ *:3;/ __filler3 :3;/' \
	-e 's/(^/(*/g' \
	-e 's-^ *__int32_t  *fds_bits\[\(.*\)\]-  __int32_t fds_bits['$${size}']-' \
	-e 's-\[ *kMIDIThruConnection_MaxEndpoints *\]-['$${kMIDIThruConnection_MaxEndpoints}']-' \
	< headers.e > headers.c )


coremidi-cffi.lisp:interface.i headers.c Makefile
	../../opt/bin/swig -cffi interface.i
	mv coremidicffi.lisp coremidi-cffi.lisp~
	( echo '(in-package "COM.INFORMATIMAGO.MACOSX.COREMIDI.CFFI")' ;\
	  sed \
		-e 's-^\([^:]*\)#\.-\1-' \
		-e 's^ &rest^ cl:\&rest^g' \
		-e 's^( *1 *<< *\([0-9][0-9]*\) *)^ #.(cl:ash 1 \1)^' \
		-e 's^ \([0-9][0-9]*\) *\([-+*/]\) *\([0-9][0-9]*\)^ #.(cl:\2 \1 \3)^' \
		-e 's^(cl:second value)^(cl:eval (cl:second value))^' \
		-e 's^(cl:defconstant ,value ,index)^(cl:eval-when (:compile-toplevel :load-toplevel :execute) (cl:defconstant ,value ,index)) do (cl:eval `(cl:defconstant ,value ,index))^' \
		< coremidi-cffi.lisp~ ) | expand -8 | sed -e 's/  *$$//' > coremidi-cffi.lisp


macerrors_h=$(shell xcrun --show-sdk-path)/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/Headers/MacErrors.h
macerrors.lisp: $(macerrors_h) midierrors.in Makefile
	( \
		echo '(in-package "COM.INFORMATIMAGO.MACOSX.COREMIDI")' ;\
		echo '(defparameter *mac-errors* (let ((table  (make-hash-table)))' ;\
		cat $(macerrors_h) midierrors.in \
		| sed -n -e 's:^  *\([a-zA-Z0-9_][a-zA-Z0-9_]*\)  *= *\(-*[0-9][0-9]*\),* *\(/\*\(.*\)\*/\)* *$$:(push (quote ("\1" "\4")) (gethash \2 table nil)):p' ;\
		echo 'table))' \
	) > macerrors.lisp


clean::
	-rm -rf headers.e headers.c \
			size size.c \
			size2 size2.dSYM

cleanall::
	-rm -f  *.bak \
			coremidi-cffi.lisp macerrors.lisp
