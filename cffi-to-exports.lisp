(defun extract-exports-from-forms (forms)
  (delete-duplicates
   (sort
    (mapcar (function string)
            (mapcan

             (lambda (form)
               (case (and (consp form) (first form))

                 ((defanonenum) (mapcar (lambda (clause)
                                          (if (listp clause)
                                              (first clause)
                                              clause))
                                        (rest form)))

                 ((cffi:defcstruct)
                  (let ((stem (second form)))
                    (mapcar (lambda (field)
                              (intern (concatenate 'string (string stem) "-"
                                                   (if (listp field)
                                                       (string (first field))
                                                       (string field)))))
                            (cddr form))))

                 ((cffi:defcvar cffi:defcfun)
                  (list (if (listp (second form))
                            (second (second form))
                            (second form))))

                 (otherwise (error "Unexpected form ~S" form))))

             forms))
    (function string<))
   :test (function string=)))




