(defpackage "COM.INFORMATIMAGO.MACOSX.COREMIDI.MIDI"
  (:use "COMMON-LISP"
        "CFFI"
        "MIDI"
        "COM.INFORMATIMAGO.MACOSX.COREMIDI")
  (:export
   "PACKET-TO-MESSAGE"
   "PACKET-LIST-TO-MESSAGES"
   "PACKET-FROM-MESSAGE"
   "PACKET-LIST-FROM-MESSAGES"))
(in-package "COM.INFORMATIMAGO.MACOSX.COREMIDI.MIDI")

(declaim (declaration stepper))

(defclass foreign-iterator ()
  ((pointer :initarg :pointer :reader   foreign-iterator-pointer)
   (size    :initarg :size    :reader   foreign-iterator-size)
   (index   :initform 0       :accessor foreign-iterator-index)))

(defmethod print-object ((iterator foreign-iterator) stream)
  (declare (stepper disable))
  (print-unreadable-object (iterator stream :identity t :type t)
    (let ((p (foreign-iterator-pointer iterator))
          (s (foreign-iterator-size    iterator))
          (i (foreign-iterator-index   iterator)))
      (format stream "~S[~A/~A]" p i s)))
  iterator)

(defgeneric foreign-iterator-ref (iterator index)
  (:method ((iterator foreign-iterator) index)
    (assert (and (<= 0 index) (< index (foreign-iterator-size iterator)))
            (index)
            "Index (~D) out of bound [0,~D[" index (foreign-iterator-size iterator))
    (cffi:mem-ref (foreign-iterator-pointer iterator) :uint8 index)))

(defgeneric foreign-iterator-position-if (predicate iterator)
  (:method  (predicate (iterator foreign-iterator))
    (loop
      :with s := (foreign-iterator-size iterator)
      :for i :from (foreign-iterator-index iterator) :below s
      :for byte := (cffi:mem-ref (foreign-iterator-pointer iterator) :uint8 i)
      :when (funcall predicate byte)
        :do (return i)
      :finally (return nil))))

(defgeneric foreign-iterator-to-vector (iterator &key vector start2 end1)
  (:method ((iterator foreign-iterator) &key vector (start2 0) end1)
    (let* ((p      (foreign-iterator-pointer iterator))
           (s      (foreign-iterator-size    iterator))
           (i      (foreign-iterator-index   iterator))
           (end1   (if end1 (min end1 s) s))
           (len    (- end1 i))
           (vector (or vector (make-octet-vector len))))
      (loop
        :for d :from start2 :below (+ start2 len)
        :do (setf (aref vector d) (cffi:mem-ref p :uint8 i))
            (incf i))
      (setf (foreign-iterator-index iterator) i)
      vector)))

(defmethod actual-read-next-byte   ((iterator foreign-iterator))
  (let ((p (foreign-iterator-pointer iterator))
        (s (foreign-iterator-size    iterator))
        (i (foreign-iterator-index   iterator)))
    (unless (< i s) (error "Cannot read beyond foreign buffer ~S" iterator))
    (values (prog1 (cffi:mem-aref p :uint8 i)
              (incf (foreign-iterator-index iterator)))
            iterator)))

(defmethod actual-unread-byte      ((iterator foreign-iterator) byte)
  (declare (ignore byte))
  (if (plusp (foreign-iterator-index iterator))
      (decf (foreign-iterator-index iterator))
      (error "Cannot ~S before start of buffer." 'actual-unread-byte))
  iterator)

(defmethod actual-write-bytes      ((iterator foreign-iterator) bytes)
  (let ((p (foreign-iterator-pointer iterator))
        (s (foreign-iterator-size    iterator))
        (i (foreign-iterator-index   iterator)))
    (unless (< i s) (error "Cannot write beyond foreign buffer ~S" iterator))
    (unwind-protect
         (map nil (lambda (byte)
                    (setf (cffi:mem-aref p :uint8 i) byte)
                    (incf i))
           bytes)
      (setf (foreign-iterator-index iterator) i)))
  iterator)


(defvar *current-sysex-status* nil)
(defvar *current-sysex-data*   nil)

(defun sysex-length (chunks len)
  (reduce (function +) chunks :key (function length) :initial-value len))

(defun concatenate-sysex-data (chunks iterator len)
  (loop
    :with size   := (sysex-length chunks len)
    :with vector := (make-octet-vector size)
    :with d      := 0
    :for chunk :in chunks
    :do (replace vector chunk :start1 d)
        (incf d (length chunk))
    :finally (return (foreign-iterator-to-vector iterator :end1 len :vector vector :start2 d))))

(defun read-sysex-from-iterator (iterator)
  (let ((len (foreign-iterator-position-if (lambda (byte) (<= 128 byte)) iterator)))
    (if len
        (prog1 (let ((byte (foreign-iterator-ref iterator len)))
                 (if (= byte #xf7) ; EOX
                     ;; sysex complete. We include the #xf0 and #xf7 bytes to the data.
                     (make-instance (if (= #xf0 *current-sysex-status*)
                                        'system-exclusive-message
                                        'authorization-system-exclusive-message)
                                    :status *current-sysex-status*
                                    :data (concatenate-sysex-data (cons (vector *current-sysex-status*)
                                                                        (nreverse *current-sysex-data*))
                                                                  iterator (1+ len)))
                     (progn ;; invalid sysex
                       (format *error-output* "~&SysEx of length ~D terminated by #x~2,'0X is ignored."
                               (sysex-length *current-sysex-data* len)
                               byte)
                       nil)))
          (setf *current-sysex-data*  '()
                *current-sysex-status* nil))
        ;; sysex not finished
        (progn (push (foreign-iterator-to-vector iterator) *current-sysex-data*)
               nil))))


(defun packet-to-messages (packet)

  ;; We cannot use MIDI to read sysex packets, because the MIDI
  ;; package is designed to read/write to MIDI files, where
  ;; system exclusive packets have a size header.
  ;;
  ;; Furthermore, we may receive incomplete (splited) sysex
  ;; packets, so we may not be able to map always one packet to
  ;; one sysex.

  ;; We cannot count on CFFI, it doesn't take into account #pragma pack(4)!
  (let* ((timeStamp (cffi:mem-ref packet :uint64))
         (iterator  (make-instance
                     'foreign-iterator
                     :pointer (cffi:inc-pointer packet (+ (cffi:foreign-type-size :uint64)
                                                          (cffi:foreign-type-size :uint16)))
                     :size    (cffi:mem-ref (cffi:inc-pointer packet (cffi:foreign-type-size :uint64))
                                            :uint16))))

    (let ((size (cffi:mem-ref (cffi:inc-pointer packet (cffi:foreign-type-size :uint64))
                              :uint16)))
      (unless (= 1 size)
        (coremidi::dump (cffi:inc-pointer packet (+ (cffi:foreign-type-size :uint64)
                                                    (cffi:foreign-type-size :uint16)))
                        size)))
    (loop
      :while (< (foreign-iterator-index iterator) (foreign-iterator-size iterator))
      ;; :for dummy := (progn (dump-foreign-iterator iterator) (terpri *error-output*))
      :for message := (if *current-sysex-status*
                          ;; read the remaining of a sysex:
                          (read-sysex-from-iterator iterator)
                          ;; read a new message:
                          (let ((status (and (< (foreign-iterator-index iterator)
                                                (foreign-iterator-size  iterator))
                                             (actual-read-next-byte iterator))))
                            (case status
                              ((nil)
                               nil)
                              ((#xf0 #xf7)
                               (setf *current-sysex-status* status
                                     *current-sysex-data*   '())
                               (read-sysex-from-iterator iterator))
                              (otherwise
                               (actual-unread-byte iterator status)
                               (read-midi-message iterator)))))
      :when message
        :do (setf (message-time message) timeStamp)
      :when message
        :collect message)))

(defun packet-list-to-messages (packet-list)
  ;; We cannot count on CFFI, it doesn't take into account #pragma pack(4)!
  (let ((numPackets (cffi:mem-ref  packet-list :unsigned-int))
        (packet     (cffi:mem-aptr packet-list :unsigned-int 1)))
    (loop
      :repeat numPackets
      :for current := packet
        :then (packet-next current)
      :nconc (packet-to-messages current))))



(defun packet-from-message (packet message)
  (let ((timestamp  (message-time message))
        (length     (message-length message)))
    (setf (cffi:mem-ref packet :uint64) timestamp)
    (setf (cffi:mem-ref (cffi:inc-pointer packet (cffi:foreign-type-size :uint64)) :uint16) length)
    (let ((bytes (cffi:inc-pointer packet (+ (cffi:foreign-type-size :uint64)
                                             (cffi:foreign-type-size :uint16)))))
      (write-midi-message (make-instance 'foreign-iterator :pointer bytes
                                                           :size length)
                          message)
      packet)))

(defun packet-list-from-messages (messages)
  (check-type messages list)            ; a list of messages.
  (assert (notany (lambda (message)
                    (typep message '(or system-exclusive-message
                                     authorization-system-exclusive-message)))
                  messages))
  (let* ((size (reduce (function +) messages
                       :initial-value (cffi:foreign-type-size :uint32)
                       :key (lambda (message)
                              (round-packet-size (+ (cffi:foreign-type-size :uint64)
                                                    (cffi:foreign-type-size :uint16)
                                                    (message-length message))))))
         (numMessages  (length messages))
         (packet-list  (cffi:foreign-alloc :uint8 :count size)))
    (packet-list-initialize packet-list)
    (setf (cffi:mem-ref packet-list :unsigned-int) numMessages)
    (loop
      :for packet := (cffi:mem-aptr packet-list :unsigned-int 1)
        :then (packet-next packet)
      :for source :in (stable-sort messages (function <) :key (function message-time))
      :do (packet-from-message packet source))
    ;; (terpri) (coremidi::dump packet-list size)
    packet-list))



;; (cffi:defcstruct MIDISysexSendRequest
;;         (destination :unsigned-int)
;;         (data :pointer)
;;         (bytesToSend :unsigned-int)
;;         (complete :unsigned-char)
;;         (reserved :pointer :count 3)
;;         (completionProc :pointer)
;;   (completionRefCon :pointer))
;;
;; (cffi:defcfun ("MIDISendSysex" MIDISendSysex) :int
;;   (request :pointer))

(defgeneric dump-foreign-iterator (iterator)
  (:method ((iterator foreign-iterator))
    (declare (stepper disable))
    (let* ((p (foreign-iterator-pointer iterator))
           (s (foreign-iterator-size iterator))
           (i (foreign-iterator-index iterator))
           (*standard-output* *error-output*))
      (loop
        :for k :from 0 :below s
        :do (when (= k i)
              (princ "["))
            (format t "~2,'0X " (cffi:mem-ref p :uint8 k))
        :finally (princ "]")))))

(defun dump-read-sysex-state (iterator)
  (declare (stepper disable))
  (let ((*standard-output* *error-output*))
    (format t "~&*current-sysex-status*        = ~A~%" *current-sysex-status*)
    (format t "~&(length *current-sysex-data*) = ~D~%" (length *current-sysex-data*))
    (format t "~&*current-sysex-data*          = ~A~%" (reverse *current-sysex-data*))
    (format t "~&iterator                      = ~A~%" iterator)
    (dump-foreign-iterator iterator)))


;;;; THE END ;;;;
