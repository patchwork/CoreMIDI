;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               coremidi.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    Wrapper tot he CoreMIDI CFFI API.
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2017-07-06 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;
;;;;    Copyright Pascal J. Bourguignon 2017 - 2017
;;;;
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************

(in-package "COM.INFORMATIMAGO.MACOSX.COREMIDI")

(deftype octet () '(unsigned-byte 8))
(defun make-octet-vector (size) (make-array size :element-type 'octet))

(defvar *coreaudio* nil)
(defun coreaudio-framework ()
  (or *coreaudio* (setf *coreaudio* (ccl:open-shared-library "/System/Library/Frameworks/CoreAudio.framework/CoreAudio"))))

(cffi:defcfun ("AudioGetCurrentHostTime" current-host-time) :uint64)

;;;
;;; Coremidi entry point. (coremidi-framework) must be called
;;; before Coremidi can be used.
;;;


(defvar *coremidi*  nil)
(defun coremidi-framework ()
  (or *coremidi*  (setf *coremidi*  (ccl:open-shared-library "/System/Library/Frameworks/CoreMIDI.framework/CoreMIDI"))))



(define-condition midi-error (error)
  ((oserr :initarg :oserr :initform 0 :accessor midi-error-oserr)
   (operation :initarg :operation :initform nil :accessor midi-error-operation))
  (:report (lambda (condition stream)
             (format stream "MIDI error, oserr = ~A,~:{ ~A (~A)~:^~% also:~} while ~S"
                     (midi-error-oserr condition)
                     (gethash (midi-error-oserr condition) *mac-errors*)
                     (midi-error-operation condition)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun plural (string-designator)
    (let ((name (string string-designator)))
      (case (char-downcase (aref name (1- (length name))))
        ((#\s)     (intern (concatenate 'string name "ES")))
        ((#\y)     (intern (concatenate 'string (subseq name 0 (1- (length name))) "IES")))
        (otherwise (intern (concatenate 'string name "S"))))))
  (defun scat (&rest string-designators)
    (intern (apply (function concatenate) 'string (mapcar (function string) string-designators)))))


(defmacro check-bounds (var start end name)
  (let ((vstart (gensym))
        (vend   (gensym)))

    `(let ((,vstart ,start)
           (,vend   ,end))
       (check-type ,var integer)
       (assert (and (<= ,vstart ,var) (< ,var  ,vend)) (,var)
               "~A is out of range [~D,~D)" ',name ,var ,vstart ,vend))))

(defmacro processing-oserr-result (funame &body one-expression)
  (assert (= 1 (length one-expression)) nil
          "~S takes only one expression as body." 'processing-oserr-result)
  (let ((vresult (gensym)))
    `(let ((,vresult ,@one-expression))
       (if (zerop ,vresult)
           (values)
           (error 'midi-error :oserr ,vresult :operation ',funame)))))

(defmacro processing-property-oserr-result (funame &body one-expression)
  (assert (= 1 (length one-expression)) nil
          "~S takes only one expression as body." 'processing-property-oserr-result)
  (let ((vresult (gensym)))
    `(let ((,vresult ,@one-expression))
       (case ,vresult
         ((0)         (values))
         ((-10835)    (return-from ,funame nil))
         (otherwise   (error 'midi-error :oserr ,vresult :operation ',funame))))))




(defmacro with-cfstring ((varname lisp-string) &body body)
  ;; cffi:*default-foreign-encoding* = :utf-8 by default.
  `(cffi:with-foreign-string (,varname ,lisp-string)
     (let ((,varname (coremidi-low::cfstringcreatewithcstring
                      ccl:+null-ptr+ ,varname
                      coremidi-low::kCFStringEncodingUTF8)))
       ,@body)))

(defun cfstring-to-lisp (cfstring)
  (unless (cffi:null-pointer-p cfstring)
    (let* ((utf-8  coremidi-low::kCFStringEncodingUTF8)
           (string (coremidi-low::CFStringGetCStringPtr cfstring utf-8)))
      (or string
          (let* ((length (coremidi-low::CFStringGetLength cfstring))
                 (size   (coremidi-low::CFStringGetMaximumSizeForEncoding length utf-8)))
            (if (zerop size)
                ""
                (cffi:with-foreign-pointer-as-string (cstring size :encoding :utf-8)
                  (coremidi-low::CFStringGetCString cfstring cstring size utf-8)
                  (cffi:foreign-string-to-lisp cstring :encoding :utf-8))))))))


(defmacro with-cfdata ((varname lisp-data) &body body)
  (let ((size (gensym))
        (vlisp-data (gensym)))
    `(let* ((,vlisp-data ,lisp-data)
            (,size (length ,vlisp-data)))
       (cffi:with-foreign-object (,varname :uint8 ,size)
         (dotimes (i ,size)
           (setf (cffi:mem-aref ,varname :uint8 i) (aref ,vlisp-data i)))
         (let ((,varname (coremidi-low::CFDataCreate (cffi:null-pointer) ,varname ,size)))
           ,@body)))))

(defun cfdata-to-lisp (cfdata)
  (let* ((size  (coremidi-low::CFDataGetLength cfdata))
         (bytes (coremidi-low::CFDataGetBytePtr Cfdata))
         (data  (make-octet-vector size)))
    (dotimes (i size data)
      (setf (aref data i) (cffi:mem-ref bytes :uint8 i)))))



;;;
;;; General operations
;;;

(defun restart ()
  (processing-oserr-result restart
    (coremidi-low::MIDIRestart)))



;;;
;;; MIDI-OBJECT
;;;


;; We wrap the MIDIClientRef et al. in structure for type checking.
;; In addition this allows us to keep some references along, such as
;; the notify-function and refcon for the client.

(defclass midi-object ()
  ((name  :initarg :name                                :type string)
   (ref   :initarg :ref   :accessor ref  :initform nil  :type (or null integer))))

(defmethod print-object ((self midi-object) stream)
  (print-unreadable-object (self stream :type t :identity t)
    (format stream ":name ~S :ref ~A" (name self) (ref self)))
  self)

(defmethod update-name-property ((self midi-object))
  (setf (midi-name self) (name self)))

(defmethod name ((self midi-object))
  (if (slot-boundp self 'name)
      (slot-value self 'name)
      (setf (slot-value self 'name) (midi-name self))))

(defmethod (setf name) (new-name (self midi-object))
  (setf (slot-value self 'name) new-name)
  (setf (midi-name self) new-name))

(defmethod properties ((self midi-object))
  (properties (class-of self)))
(defmethod properties ((self class))
  (properties (class-name self)))
(defmethod properties ((class-name symbol))
  (reduce (function append)
          (cons (get class-name 'properties)
                (mapcar (function properties)
                        (closer-mop:class-direct-superclasses (find-class class-name))))
          :initial-value '()
          :from-end t))

(defgeneric refcon (object)
  (:method ((self t)) nil))

(defclass refcon-mixin ()
  ((refcon :initarg :refcon   :accessor refcon  :initform nil)))

(defvar *refcon-index* #x00400000)
(defun generate-refcon ()
  (cffi:foreign-alloc :ulong :initial-element (incf *refcon-index* 8)))

(defclass client (midi-object refcon-mixin)
  ((notify-function :initarg :notify-function
                    :accessor client-notify-function
                    :initform nil
                    :type (or null function symbol))))

(defclass port (midi-object refcon-mixin)
  ((read-function :initarg :read-function
                  :accessor port-read-function
                  :initform nil
                  :type (or null function symbol))
   (srcconnrefcon :initarg :srcconnrefcon
                  :accessor port-srcconnrefcon
                  :initform nil)
   (client :initarg :client
           :accessor port-client
           :initform nil
           :type (or null client))))

(defclass device (midi-object)
  ())

(defclass entity (midi-object)
  ())

(defclass endpoint (midi-object)
  ((client :initarg :client
           :accessor endpoint-client
           :initform nil
           :type (or null client))))

(defclass source (endpoint)
  ())

(defclass destination (endpoint refcon-mixin)
  ((read-function :initarg :read-function
                  :accessor endpoint-read-function
                  :initform nil
                  :type (or null function symbol))))



(defmacro define-register (name)
  (let ((names  (scat "*" (plural name) "*")))
    `(progn
       (defvar ,names '())
       (defun ,(scat 'register- name) (,name)
         (push ,name ,names))
       (defun ,(scat 'find- name '-for-refcon) (refcon)
         (find refcon ,names :key (function refcon)))
       (defun ,(scat 'intern- name) (ref)
         (or (find ref ,names :key (function ref))
             (let ((new (make-instance ',name :ref ref)))
               (push new ,names)
               new)))
       (defun ,(scat 'unintern- name) (ref)
         (setf ,names (delete (find ref ,names :key (function ref)) ,names)))
       (defun ,(scat 'unregister- name) (,name)
         (setf ,names (delete ,name ,names)))
       ',name)))


(defmacro generate-dispose (name fun)
  `(defun ,(scat name '-dispose) (,name)
     (check-type ,name ,name)
     (let ((ref (ref ,name)))
       (when ref
         (processing-oserr-result ,(scat name '-dispose)
           (,fun ref))))
     (setf (ref    ,name) nil
           (refcon ,name) nil)
     (,(scat 'unregister- name) ,name)
     (values)))


(defmacro define-object-list (class signature count-function get-function)
  (let* ((name                  (if (listp signature)
                                    (first signature)
                                    signature))
         (arguments             (if (listp signature)
                                    (rest signature)
                                    '()))
         (fcount                (scat name '-count))
         (fref                  (scat name '-get))
         (fnames                (plural name))
         (flat-arguments        (mapcar (lambda (argument)
                                          (if (listp argument)
                                              (first argument)
                                              argument))
                                        arguments))
         (referenced-arguments  (mapcar (lambda (argument)
                                          (if (listp argument)
                                              `(ref ,(first argument))
                                              argument))
                                        arguments)))
    `(progn
       (defun ,fcount (,@flat-arguments) (,count-function ,@referenced-arguments))
       (defun ,fref   (,@flat-arguments index)
         (let ((count (,count-function ,@referenced-arguments)))
           (check-bounds index 0 count ,(format nil "~:(~A~) index" name)))
         (,(scat 'intern- class) (,get-function ,@referenced-arguments index)))
       (defun ,fnames (,@flat-arguments)
         (loop :for i :below (,count-function ,@referenced-arguments)
               :collect (,(scat 'intern- class) (,get-function ,@referenced-arguments i)))))))


;;;
;;; client
;;;

(define-register client)
(generate-dispose client coremidi-low::MIDIClientDispose)


;; The notify-callback function uses the refcon pointer to locate the
;; client structure and thus the lisp notify-function closure to call.

(cffi:defcallback notify-callback
    :void ((message :pointer #|const MIDINotification *|#)
           (refcon :pointer))
  (let ((client (find-client-for-refcon (cffi:pointer-address refcon))))
    (when client
      (let ((fun (client-notify-function client)))
        (funcall fun message))))
  (values))


(defun client-create (name notify-function)
  (check-type name string)
  (check-type notify-function (or function symbol))
  (let* ((refcon (generate-refcon))
         (client (make-instance 'client :name name
                                        :refcon refcon
                                        :notify-function notify-function)))
    (register-client client)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (processing-oserr-result client-create
          (coremidi-low::MIDIClientCreate name (cffi:callback notify-callback) refcon out)))
      (setf (ref client) (cffi:mem-ref out :uint))
      client)))

(defun clients ()
  (copy-list *clients*))



;;;
;;; port
;;;

(define-register port)
(generate-dispose port coremidi-low::MIDIPortDispose)

(defvar *bad-refcons* '())
(defun %port-read-callback (pktlist refcon srcConnRefCon)
  ;; We use a separate lisp function for the callback so we may use trace on it…
  (handler-bind ((error (lambda (condition)
                          (finish-output *standard-output*)
                          (terpri *error-output*)
                          #+ccl (format *error-output* "~&~80,,,'-<~>~&~{~A~%~}~80,,,'-<~>~&"
                                        (ccl::backtrace-as-list))
                          (format *error-output* "~%ERROR: ~A~%" condition)
                          (finish-output *error-output*)
                          (return-from %port-read-callback))))
    (let ((port (find-port-for-refcon refcon)))
      (if port
          (let ((fun (port-read-function port)))
            (if fun
                (funcall fun pktlist srcConnRefCon)
                (format *trace-output* "No port-read-function for port ~S~%" port)))
          (unless (member port *bad-refcons*)
            (push refcon *bad-refcons*)
            (format *trace-output* "No port for refcon ~S~%" refcon))))))

(cffi:defcallback port-read-callback
    :void ((pktlist       :pointer #|const MIDIPacketList *|#)
           (refcon        :pointer)
           (srcConnRefCon :pointer))
  (%port-read-callback pktlist refcon srcConnRefCon)
  (values))

(defun input-port-create (client name read-function)
  (check-type client client)
  (check-type name string)
  (check-type read-function (or function symbol))
  (let* ((refcon (generate-refcon))
         (port   (make-instance 'port :name name
                                      :client client
                                      :refcon refcon
                                      :read-function read-function)))
    (register-port port)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (processing-oserr-result input-port-create
          (coremidi-low::MIDIInputPortCreate (ref client)
                                             name
                                             (cffi:callback port-read-callback)
                                             refcon
                                             out)))
      (setf (ref port) (cffi:mem-ref out :uint))
      port)))

(defun output-port-create (client name)
  (check-type client client)
  (check-type name string)
  (let* ((port   (make-instance 'port :name name
                                      :client client)))
    (register-port port)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (processing-oserr-result output-port-create
          (coremidi-low::MIDIOutputPortCreate (Ref client) name
                                              out)))
      (setf (ref port) (cffi:mem-ref out :uint))
      port)))

(defun port-connect-source (port source connRefCon)
  (check-type port port)
  (check-type source endpoint)
  ;; (check-type connRefCon)
  (setf (port-srcconnrefcon port) connRefCon)
  (processing-oserr-result port-connect-source
    (coremidi-low::MIDIPortConnectSource (ref port) (ref source) connRefCon)))

(defun port-disconnect-source (port source)
  (check-type port port)
  (check-type source endpoint)
  (unwind-protect
       (processing-oserr-result port-disconnect-source
         (coremidi-low::MIDIPortDisconnectSource (ref port) (ref source)))
    (setf (port-srcconnrefcon port) nil)))

(defun ports ()
  (copy-list *ports*))


;;;
;;; devices
;;;

(define-register device)
(define-object-list device (device)
  coremidi-low::MIDIGetNumberOfDevices
  Coremidi-low::MIDIGetDevice)


(defun device-add-entity (device name embedded num-source-endpoints num-destination-endpoints)
  (check-type device device)
  (check-type name string)
  (check-type num-source-endpoints (integer 0))
  (check-type num-destination-endpoints (integer 0))
  (cffi:with-foreign-object (entity :pointer)
    (with-cfstring (name name)
      (processing-oserr-result device-add-entity
        (coremidi-low::MIDIDeviceAddEntity (ref device) name (if embedded 1 0)
                                           num-source-endpoints num-destination-endpoints
                                           entity)))
    (intern-entity (cffi:mem-ref entity :pointer))))


(defun device-remove-entity (device entity)
  (check-type device device)
  (check-type entity entity)
  (processing-oserr-result device-remove-entity
    (coremidi-low::MIDIDeviceRemoveEntity (ref device) (ref entity)))
  (unregister-entity entity))


(defun setup-add-device (device)
  (check-type device device)
  (processing-oserr-result setup-add-device
    (coremidi-low::MIDISetupAddDevice (ref device))))

(defun setup-remove-device (device)
  (check-type device device)
  (processing-oserr-result setup-add-device
    (coremidi-low::MIDISetupRemoveDevice (ref device))))



;;;
;;; entities
;;;

(defun entity-device (entity)
  (cffi:with-foreign-object (out :uint 1)
    (processing-oserr-result output-port-create
      (coremidi-low::MIDIEntityGetDevice (ref entity) out))
    (intern-device (cffi:mem-ref out :uint))))

(define-register entity)

(define-object-list entity (device-entity (device device))
  coremidi-low::MIDIDeviceGetNumberOfEntities
  coremidi-low::MIDIDeviceGetEntity)

(define-object-list endpoint (entity-source (entity entity))
  coremidi-low::MIDIEntityGetNumberOfSources
  Coremidi-low::MIDIEntityGetSource)

(define-object-list endpoint (entity-destination (entity entity))
  coremidi-low::MIDIEntityGetNumberOfDestinations
  coremidi-low::MIDIEntityGetDestination)


(define-object-list endpoint (source)
  coremidi-low::MIDIGetNumberOfSources
  coremidi-low::MIDIGetSource)

(define-object-list endpoint (destination)
  coremidi-low::MIDIGetNumberOfDestinations
  coremidi-low::MIDIGetDestination)


(defun entity-add-or-remove-endpoints (entity num-source-endpoints num-destination-endpoints)
  (check-type entity entity)
  (check-type num-source-endpoints (integer 0))
  (check-type num-destination-endpoints (integer 0))
  (processing-oserr-result entity-add-or-remove-endpoints
    (coremidi-low::MIDIEntityAddOrRemoveEndpoints (ref entity)
                                                  num-source-endpoints
                                                  num-destination-endpoints)))





;;;
;;; Endpoints
;;;

(defun endpoint-entity (endpoint)
  (cffi:with-foreign-object (out :uint 1)
    (processing-oserr-result output-port-create
      (let ((errno (coremidi-low::MIDIEndpointGetEntity (ref endpoint) out)))
        (if (= errno coremidi-low::kMIDIObjectNotFound)
            (return-from endpoint-entity nil)
            errno)))
    (intern-entity (cffi:mem-ref out :uint))))

(define-register endpoint)
(generate-dispose endpoint coremidi-low::MIDIEndpointDispose)

(defun endpoints ()
  (copy-list *endpoints*))

(cffi:defcallback endpoint-read-callback
    :void ((pktlist       :pointer #|const MIDIPacketList *|#)
           (refcon        :pointer)
           (srcConnRefCon :pointer))
  (let ((endpoint (find-endpoint-for-refcon (cffi:pointer-address refcon))))
    (when endpoint
      (let ((fun (endpoint-read-function endpoint)))
        (when fun
          (funcall fun pktlist srcConnRefCon)))))
  (values))


(defun destination-create (client name read-function)
  (check-type client client)
  (check-type name string)
  (check-type read-function (or function symbol))
  (let* ((refcon      (generate-refcon))
         (destination (make-instance 'destination :name name
                                                  :client client
                                                  :refcon refcon
                                                  :read-function read-function)))
    (register-endpoint destination)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (processing-oserr-result destination-create
          (coremidi-low::MIDIDestinationCreate (ref client) name (cffi:callback endpoint-read-callback) (cffi:make-pointer refcon)
                                               out)))
      (setf (ref destination) (cffi:mem-ref out :uint))
      destination)))


(defun source-create (client name)
  (check-type client client)
  (check-type name string)
  (let ((source (make-instance 'source :name name
                                       :client client)))
    (register-endpoint source)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (processing-oserr-result source-create
          (coremidi-low::MIDISourceCreate (ref client) name
                                          out)))
      (setf (ref source) (cffi:mem-ref out :uint))
      source)))


;;;
;;; External Devices
;;;

(define-object-list device (external-device)
  coremidi-low::MIDIGetNumberOfExternalDevices
  Coremidi-low::MIDIGetExternalDevice)


(defun external-device-create (name manufacturer model)
  (check-type name string)
  (check-type manufacturer string)
  (check-type model string)
  (let ((external-device (make-instance 'device :name name)))
    (register-device external-device)
    (cffi:with-foreign-object (out :uint 1)
      (with-cfstring (name name)
        (with-cfstring (manufacturer manufacturer)
          (with-cfstring (model model)
            (processing-oserr-result external-device-create
              (coremidi-low::MIDIExternalDeviceCreate name manufacturer model
                                                      out)))))
      (setf (ref external-device) (cffi:mem-ref out :uint))
      external-device)))


(defun setup-add-external-device (device)
  (check-type device device)
  (processing-oserr-result setup-add-device
    (coremidi-low::MIDISetupAddExternalDevice (ref device))))

(defun setup-remove-external-device (device)
  (check-type device device)
  (processing-oserr-result setup-add-device
    (coremidi-low::MIDISetupRemoveExternalDevice (ref device))))


;;;
;;; Properties
;;;


(defmacro define-property (classes fname pname type)
  (check-type classes (or symbol list))
  (check-type fname symbol)
  ;; (check-type pname string)
  (check-type type  symbol)
  (let ((classes (if (atom classes)
                     (list classes)
                     classes)))
    `(progn
       ,@(mapcar (lambda (class)

                   `(progn

                      (pushnew ',fname (get ',class 'properties '()))

                      (defmethod ,fname ((object ,class))
                        ,(ecase type
                           (boolean    `(let ((value (get-integer (ref object) ,pname)))
                                          (and value (not (zerop value)))))
                           (integer    `(get-integer    (ref object) ,pname))
                           (string     `(get-string     (ref object) ,pname))
                           (data       `(get-data       (ref object) ,pname))
                           (dictionary `(get-dictionary (ref object) ,pname))))

                      (defmethod  (setf ,fname) (new-value (object ,class))
                        (check-type new-value ,(ecase type
                                                 (boolean    't)
                                                 (integer    '(signed-byte 32))
                                                 (string     'string)
                                                 (data       '(vector (unsigned-byte 8)))
                                                 (dictionary 'hash-table)))
                        ,(ecase type
                           (boolean    `(set-integer    (ref object) ,pname (if new-value 1 0)))
                           (integer    `(set-integer    (ref object) ,pname new-value))
                           (string     `(set-string     (ref object) ,pname new-value))
                           (data       `(set-data       (ref object) ,pname new-value))
                           (dictionary `(set-dictionary (ref object) ,pname new-value))))))
                 classes)

       ',fname)))



(defun get-integer (obj property)
  (cffi:with-foreign-object (value :int)
    (processing-property-oserr-result get-integer
      (coremidi-low::MIDIObjectGetIntegerProperty obj property value))
    (cffi:mem-ref value :int)))

(defun set-integer (obj property value)
  (processing-oserr-result set-integer
    (coremidi-low::MIDIObjectSetIntegerProperty obj property value)))

(defun get-string (obj property)
  (cffi:with-foreign-object (value :pointer)
    (processing-property-oserr-result get-string
      (coremidi-low::MIDIObjectGetStringProperty obj property value))
    (cfstring-to-lisp (cffi:mem-ref value :pointer))))

(defun set-string (obj property value)
  (cffi:with-foreign-string (string value)
    (processing-oserr-result set-string
      (coremidi-low::MIDIObjectSetStringProperty obj property string))))

(defun get-data (obj property)
  (cffi:with-foreign-object (value :pointer)
    (processing-property-oserr-result get-data
      (coremidi-low::MIDIObjectGetDataProperty obj property value))
    (cfdata-to-lisp (cffi:mem-ref value :pointer))))

(defun set-data (obj property value)
  (with-cfdata (data value)
    (processing-oserr-result set-data
      (coremidi-low::MIDIObjectSetDataProperty obj property data))))

(defun get-dictionary (obj property)
  (cffi:with-foreign-object (value :pointer)
    (processing-property-oserr-result get-dictionary
      (coremidi-low::MIDIObjectGetDictionaryProperty obj property value))
    (cfdictionary-to-lisp (cffi:mem-ref value :pointer))))

(defun set-dictionary (obj property value)
  (processing-oserr-result set-dictionary
    (coremidi-low::MIDIObjectSetDictionaryProperty obj property value)))


;; (cffi:defcfun ("MIDIObjectGetProperties" MIDIObjectGetProperties) :int
;;   (obj :unsigned-int)
;;   (outProperties :pointer)
;;   (deep :unsigned-char))
;;
;; (cffi:defcfun ("MIDIObjectRemoveProperty" MIDIObjectRemoveProperty) :int
;;   (obj :unsigned-int)
;;   (propertyID :pointer))



(define-property midi-object          midi-name                      coremidi-low::kMIDIPropertyName                        String)
(define-property midi-object          unique-id                      coremidi-low::kMIDIPropertyUniqueID                    Integer)
(define-property midi-object          max-sysex-speed                coremidi-low::kMIDIPropertyMaxSysExSpeed               Integer)
(define-property midi-object          advance-schedule-time-musec    coremidi-low::kMIDIPropertyAdvanceScheduleTimeMuSec    Integer)
(define-property midi-object          connection-unique-id           coremidi-low::kMIDIPropertyConnectionUniqueID          Data)
(define-property midi-object          offline                        coremidi-low::kMIDIPropertyOffline                     Boolean)
(define-property midi-object          private                        coremidi-low::kMIDIPropertyPrivate                     Boolean)
(define-property midi-object          driver-owner                   coremidi-low::kMIDIPropertyDriverOwner                 String)
(define-property midi-object          factory-patch-name-file        coremidi-low::kMIDIPropertyFactoryPatchNameFile        Data)
(define-property midi-object          user-patch-name-file           coremidi-low::kMIDIPropertyUserPatchNameFile           Data)
(define-property midi-object          name-configuration             coremidi-low::kMIDIPropertyNameConfiguration           Dictionary)
(define-property midi-object          driver-version                 coremidi-low::kMIDIPropertyDriverVersion               Integer)
(define-property midi-object          display-name                   coremidi-low::kMIDIPropertyDisplayName                 String)

(define-property (endpoint entity)    is-Embedded-Entity             coremidi-low::kMIDIPropertyIsEmbeddedEntity            Boolean)
(define-property (endpoint entity)    is-Broadcast                   coremidi-low::kMIDIPropertyIsBroadcast                 Boolean)

(define-property (device   endpoint)  device-ID                      coremidi-low::kMIDIPropertyDeviceID                    Integer)
(define-property (device   endpoint)  manufacturer                   coremidi-low::kMIDIPropertyManufacturer                String)
(define-property (device   endpoint)  model                          coremidi-low::kMIDIPropertyModel                       String)

(define-property (device   entity)    supports-General-MIDI          coremidi-low::kMIDIPropertySupportsGeneralMIDI         Boolean)
(define-property (device   entity)    supports-MMC                   coremidi-low::kMIDIPropertySupportsMMC                 Boolean)
(define-property (device   entity)    can-Route                      coremidi-low::kMIDIPropertyCanRoute                    Boolean)
(define-property (device   entity)    receives-Clock                 coremidi-low::kMIDIPropertyReceivesClock               Boolean)
(define-property (device   entity)    receives-MTC                   coremidi-low::kMIDIPropertyReceivesMTC                 Boolean)
(define-property (device   entity)    receives-Notes                 coremidi-low::kMIDIPropertyReceivesNotes               Boolean)
(define-property (device   entity)    receives-Program-Changes       coremidi-low::kMIDIPropertyReceivesProgramChanges      Boolean)
(define-property (device   entity)    receives-Bank-Select-MSB       coremidi-low::kMIDIPropertyReceivesBankSelectMSB       Boolean)
(define-property (device   entity)    receives-Bank-Select-LSB       coremidi-low::kMIDIPropertyReceivesBankSelectLSB       Boolean)
(define-property (device   entity)    transmits-Clock                coremidi-low::kMIDIPropertyTransmitsClock              Boolean)
(define-property (device   entity)    transmits-MTC                  coremidi-low::kMIDIPropertyTransmitsMTC                Boolean)
(define-property (device   entity)    transmits-Notes                coremidi-low::kMIDIPropertyTransmitsNotes              Boolean)
(define-property (device   entity)    transmits-Program-Changes      coremidi-low::kMIDIPropertyTransmitsProgramChanges     Boolean)
(define-property (device   entity)    transmits-Bank-Select-MSB      coremidi-low::kMIDIPropertyTransmitsBankSelectMSB      Boolean)
(define-property (device   entity)    transmits-Bank-Select-LSB      coremidi-low::kMIDIPropertyTransmitsBankSelectLSB      Boolean)
(define-property (device   entity)    pan-Disrupts-Stereo            coremidi-low::kMIDIPropertyPanDisruptsStereo           Boolean)
(define-property (device   entity)    is-Sampler                     coremidi-low::kMIDIPropertyIsSampler                   Boolean)
(define-property (device   entity)    is-Drum-Machine                coremidi-low::kMIDIPropertyIsDrumMachine               Boolean)
(define-property (device   entity)    is-Mixer                       coremidi-low::kMIDIPropertyIsMixer                     Boolean)
(define-property (device   entity)    is-Effect-Unit                 coremidi-low::kMIDIPropertyIsEffectUnit                Boolean)
(define-property (device   entity)    max-Receive-Channels           coremidi-low::kMIDIPropertyMaxReceiveChannels          Integer)
(define-property (device   entity)    max-Transmit-Channels          coremidi-low::kMIDIPropertyMaxTransmitChannels         Integer)
(define-property (device   entity)    supports-Show-Control          coremidi-low::kMIDIPropertySupportsShowControl         Boolean)

(define-property device               image                          coremidi-low::kMIDIPropertyImage                       String)
(define-property device               driver-Device-Editor-App       coremidi-low::kMIDIPropertyDriverDeviceEditorApp       String)

(define-property endpoint             receive-Channels               coremidi-low::kMIDIPropertyReceiveChannels             Integer)
(define-property endpoint             transmit-Channels              coremidi-low::kMIDIPropertyTransmitChannels            Integer)



;;;
;;; MIDI I/O
;;;

(defun send (port destination packet-list)
  (check-type port port)
  (check-type destination endpoint)
  (coremidi-low::MIDISend (ref port) (ref destination) packet-list))

(defun send-sysex (request)
  (coremidi-low::MIDISendSysex request))

(defun flush-output (destination)
  "This doesn't finish the MIDI output, this clears the output buffers, so that queued output is aborted."
  (check-type destination endpoint) ; we'd want destination, but not all destinations are identified as such, they may be instanciated as endpoints.
  (processing-oserr-result flush-output
    (coremidi-low::MIDIFlushOutput (ref destination))))


(defun received (source packet-list)
  ;; To be called by the driver when the source produced the packet-list
  ;; or by the client when it's own source produced the packet-list.
  (check-type source endpoint) ; we'd want source, but not all sources are identified as such, they may be instanciated as endpoints.
  (processing-oserr-result received
    (coremidi-low::MIDIReceived (ref source) packet-list)))


;;;
;;; packet lists
;;;



(defvar *sysex-completions* '())

(defun extract-sysex-completion-for-refcon (refcon)
  (let ((completion (find refcon *sysex-completions* :key (function first))))
    (when completion
      (setf *sysex-completions* (delete completion *sysex-completions*)))
    completion))

(cffi:defcallback sysex-completion-callback
    :void ((request :pointer #| MIDISysexSendRequest *|#))
  (let* ((low-refcon (cffi:pointer-address
                      (cffi:with-foreign-slots ((coremidi-low::completionRefCon) request
                                                (:struct coremidi-low::MIDISysexSendRequest))
                        coremidi-low::completionRefCon)))
         (completion (extract-sysex-completion-for-refcon low-refcon)))
    (when completion
      (funcall (second completion) request (third completion))))
  (values))


(defun sysex-request (destination data completion refcon)

  ;; We cannot count on CFFI.
  (let* ((bytes-to-send (length data))
         (request (cffi:foreign-alloc :uint8 :count 40 :initial-element 0))
         (buffer  (cffi:foreign-alloc :uint8 :count bytes-to-send)))
    (loop
      :for i :below bytes-to-send
      :do (setf (cffi:mem-aref buffer :uint8 i) (aref data i)))

    (setf (cffi:mem-ref (cffi:inc-pointer request  0) :uint32)  (ref destination)
          (cffi:mem-ref (cffi:inc-pointer request  8) :pointer) buffer
          (cffi:mem-ref (cffi:inc-pointer request 16) :uint32)  bytes-to-send
          (cffi:mem-ref (cffi:inc-pointer request 20) :uint8)   0
          (cffi:mem-ref (cffi:inc-pointer request 24) :pointer) (if completion
                                                                    (cffi:callback sysex-completion-callback)
                                                                    (cffi:null-pointer))
          (cffi:mem-ref (cffi:inc-pointer request 32) :pointer) (if completion
                                                                    (let ((low-refcon (generate-refcon)))
                                                                      (push (list
                                                                             low-refcon
                                                                             completion
                                                                             refcon)
                                                                            *sysex-completions*)
                                                                      (cffi:make-pointer low-refcon))
                                                                    (cffi:null-pointer)))

    ;; (dump buffer bytes-to-send)
    ;; (dump request 40)
    request))


(defun packet-to-lisp (packet)
  ;; We cannot count on CFFI, it doesn't take into account #pragma pack(4)!
  (let ((timeStamp (cffi:mem-ref packet :uint64))
        (length    (cffi:mem-ref (cffi:inc-pointer  packet (cffi:foreign-type-size :uint64)) :uint16))
        (bytes     (cffi:inc-pointer  packet (+ (cffi:foreign-type-size :uint64)
                                                (cffi:foreign-type-size :uint16)))))
    (let ((data (make-octet-vector length)))
      (dotimes (i length)
        (setf (aref data i) (cffi:mem-aref bytes :uint8 i)))
      (cons timeStamp data))))

(defun packet-list-to-lisp (packet-list)
  ;; We cannot count on CFFI, it doesn't take into account #pragma pack(4)!
  (let ((numPackets (cffi:mem-ref  packet-list :unsigned-int))
        (packet     (cffi:mem-aptr packet-list :unsigned-int 1)))
    (loop
      :repeat numPackets
      :for current := packet
        :then (packet-next current)
      :collect (packet-to-lisp current))))


(defun packet-next (packet)
  ;; We cannot count on CFFI, it doesn't take into account #pragma pack(4)!
  (let ((length    (cffi:mem-ref (cffi:inc-pointer packet (cffi:foreign-type-size :uint64)) :uint16))
        (bytes     (cffi:inc-pointer packet (+ (cffi:foreign-type-size :uint64)
                                               (cffi:foreign-type-size :uint16)))))
    #+(or arm arm64)
    (cffi:make-pointer (logand (+ (cffi:pointer-address bytes) length 3) (lognot 3)))
    #-(or arm arm64)
    (cffi:inc-pointer bytes length)))


(defun packet-list-initialize (packet-list)
  (coremidi-low::MIDIPacketListInit packet-list))

(defun packet-list-add (packet-list list-size
                        current-packet time data-size data)
  (coremidi-low::MIDIPacketListAdd packet-list list-size
                                   current-packet time data-size data))

(defun round-packet-size (size)
  #+(or arm arm64)  (logand (+ size 3) (lognot 3))
  #-(or arm arm64)  size)


(defun fill-packet-from-lisp (packet lisp-packet)
  (let ((timestamp  (car lisp-packet))
        (data       (cdr lisp-packet)))
    (setf (cffi:mem-ref packet :uint64) timestamp)
    (setf (cffi:mem-ref (cffi:inc-pointer packet (cffi:foreign-type-size :uint64)) :uint16)
          (length data))
    (let ((bytes (cffi:inc-pointer packet (+ (cffi:foreign-type-size :uint64)
                                             (cffi:foreign-type-size :uint16)))))
      (dotimes (i (length data))
        (setf (cffi:mem-aref bytes :uint8 i) (aref data i)))
      packet)))


(defun packet-list-from-lisp (packets)
  (check-type packets list) ; a list of conses (timestamp . byte-vector)
  (let* ((size (reduce (function +) packets
                       :initial-value (cffi:foreign-type-size :uint32)
                       :key (lambda (packet)
                              (round-packet-size (+ (cffi:foreign-type-size :uint64)
                                                    (cffi:foreign-type-size :uint16)
                                                    (length (cdr packet)))))))
         (numPackets  (length packets))
         (packet-list (cffi:foreign-alloc :uint8 :count size)))
    (packet-list-initialize packet-list)
    (setf (cffi:mem-ref packet-list :unsigned-int) numPackets)
    (loop
      :for packet := (cffi:mem-aptr packet-list :unsigned-int 1)
        :then (packet-next packet)
      :for source :in (stable-sort packets (function <) :key (function car))
      :do (fill-packet-from-lisp packet source))
    ;; (dump packet-list size)
    packet-list))


;;;
;;; Utility.
;;;


(defun find-device-named (name)
  (find name (devices) :key (function name) :test (function equalp)))

(defun find-external-device-named (name)
  (find name (external-devices) :key (function name) :test (function equalp)))

(defun find-entity-named (device-name entity-name)
  (find entity-name
        (device-entities (or (find-device-named device-name)
                             (find-external-device-named device-name)
                             (error "There is no device named ~S" device-name)))
        :key (function name)
        :test (function equalp)))

(defun find-endpoint-named (device-name entity-name endpoint-name)
  (let ((entities (find-entity-named device-name entity-name)))
    (or (find endpoint-name
              (entity-sources entities)
              :key (function name)
              :test (function equalp))
        (find endpoint-name
              (entity-destinations entities)
              :key (function name)
              :test (function equalp)))))

(defun find-source-named (device-name entity-name endpoint-name)
  (let ((entities (find-entity-named device-name entity-name)))
    (find endpoint-name
          (entity-sources entities)
          :key (function name)
          :test (function equalp))))

(defun find-destination-named (device-name entity-name endpoint-name)
  (let ((entities (find-entity-named device-name entity-name)))
    (find endpoint-name
          (entity-destinations entities)
          :key (function name)
          :test (function equalp))))



(defun connected-objects (object)
  (connected-objects-from-unique-ids (connection-unique-id object)))

(defgeneric entity (object)
  (:method ((self entity))   self)
  (:method ((self endpoint)) (endpoint-entity self)))

(defgeneric device (object)
  (:method ((self device)))
  (:method ((self entity))   (entity-device self))
  (:method ((self endpoint))
    (let ((entity (entity self)))
      (and entity (entity-device entity)))))

(defun connected-devices (object)
  (mapcan (lambda (object)
            (let ((device (device object)))
              (when device (list device))))
          (connected-objects object)))

;; (connected-devices (find-endpoint-named "UM-ONE" "UM-ONE" "UM-ONE"))
;; ;; (#<device :name "Korg DW-8000" :ref 194814030 #x3020031C136D>
;; ;;  #<device :name "Korg DSS-1" :ref 194814026 #x3020031C13BD>
;; ;;  #<device :name "Korg MS2000R" :ref 194814022 #x3020031C140D>)
;;
;; (connected-devices
;;  (first (entity-sources (first (device-entities (find-external-device-named "Korg DW-8000"))))))
;; ;; (#<device :name "UM-ONE" :ref 194814003 #x3020031A4CCD>)

(defun object-type (type)
 (ecase type
   ((#.coremidi-low::kMIDIObjectType_Device)               :device)
   ((#.coremidi-low::kMIDIObjectType_ExternalDevice)       :external-device)
   ((#.coremidi-low::kMIDIObjectType_Entity)               :entity)
   ((#.coremidi-low::kMIDIObjectType_ExternalEntity)       :external-entity)
   ((#.coremidi-low::kMIDIObjectType_Source)               :source)
   ((#.coremidi-low::kMIDIObjectType_Destination)          :destination)
   ((#.coremidi-low::kMIDIObjectType_ExternalSource)       :external-source)
   ((#.coremidi-low::kMIDIObjectType_ExternalDestination)  :external-destination)
   (otherwise                                              :other)))

(defun object-by-unique-id (id)
  (cffi:with-foreign-object (object :uint 1)
    (cffi:with-foreign-object (type :int32 1)
      (processing-oserr-result find-object-by-unique-id
        (MIDIObjectFindByUniqueID id object type))
      (let ((object-ref (cffi:mem-ref object :uint))
            (type       (cffi:mem-ref type   :int32)))
        (values object-ref type)))))

(defun find-object-by-unique-id (id)
  (cffi:with-foreign-object (object :uint 1)
    (cffi:with-foreign-object (type :int32 1)
      (processing-oserr-result find-object-by-unique-id
        (MIDIObjectFindByUniqueID id object type))
      (let ((object-ref (cffi:mem-ref object :uint))
            (type       (cffi:mem-ref type   :int32)))
        (funcall (ecase type
                   ((#.coremidi-low::kMIDIObjectType_Device
                     #.coremidi-low::kMIDIObjectType_ExternalDevice)
                    (function intern-device))
                   ((#.coremidi-low::kMIDIObjectType_Entity
                     #.coremidi-low::kMIDIObjectType_ExternalEntity)
                    (function intern-entity))
                   ((#.coremidi-low::kMIDIObjectType_Source
                     #.coremidi-low::kMIDIObjectType_Destination
                     #.coremidi-low::kMIDIObjectType_ExternalSource
                     #.coremidi-low::kMIDIObjectType_ExternalDestination)
                    (function intern-endpoint)))
                 object-ref)))))

(defun connected-objects-from-unique-ids (byte-vector)
  (loop :for i :from 0 :by 4 :below (length byte-vector)
        :for id = (let ((n (dpb (aref byte-vector i) (byte 8 24)
                              (dpb (aref byte-vector (1+ i)) (byte 8 16)
                                   (dpb (aref byte-vector (+ 2 i)) (byte 8 8)
                                        (aref byte-vector (+ 3 i)))))))
                    (if (< n #.(expt 2 31))
                        n
                        (- n #.(expt 2 32))))
        :for object := (find-object-by-unique-id id)
        :collect object))


(defun dump (pointer size)
  (loop
    :repeat size
    :for i :from 0
    :when (zerop (mod i 16))
      :do (format t "~&~16,'0X: " (+ i (cffi:pointer-address pointer)))
    :do (format t "~2,'0X " (cffi:mem-aref pointer :uint8 i))
    :finally (terpri)))



(cffi:defcfun ("CFStringGetCStringPtr" CFStringGetCStringPtr) :pointer
  (string :pointer)
  (encoding :unsigned-int))
(cffi:defcfun ("CFStringGetLength" CFStringGetLength) :unsigned-int
  (string :pointer))
(cffi:defcfun ("CFStringGetCString" CFStringGetCString) :bool
  (string :pointer)
  (buffer :pointer)
  (size :unsigned-int)
  (encoding :unsigned-int))




;;;; THE END ;;;;
