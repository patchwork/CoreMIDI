#define __inline
#define __inline__
#define __attribute__(x)
#define __asm(x)
#define __asm__(x)

#define _Nullable
#define _Nonnull

#include <CoreMIDI/CoreMIDI.h>
