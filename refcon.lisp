(defvar *refcon-from-lisp* (make-hash-table))
(defvar *refcon-to-lisp*   (make-hash-table))
(defvar *refcon-index* 0)

(defun refcon-from-lisp (lisp-object)
  (or (gethash lisp-object *refcon-from-lisp*)
      (let ((ref (cffi:foreign-alloc :ulong :initial-element (incf *refcon-index*))))
        (setf (gethash *refcon-index* *refcon-to-lisp*) lisp-object
              (gethash lisp-object *refcon-from-lisp*) ref))))

(defun refcon-to-lisp (refcon)
  (if (cffi:null-pointer-p refcon)
      nil
      (gethash (cffi:memref refcon :ulong) *refcon-to-lisp*)))

(defun free-refcon (refcon)
  (remhash (refcon-to-lisp refcon)     *refcon-from-lisp*)
  (remhash (cffi:memref refcon :ulong) *refcon-to-lisp*)
  (cffi:foreign-free refcon))

