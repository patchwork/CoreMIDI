#include <stdio.h>
#include <CoreMIDI/CoreMIDI.h>
// #define offsetof(s,field) (((char*)&(s.field))-((char*)&(s)))
int main(void){
    struct MIDISysexSendRequest r;
    const char* format="%-30s : %8zu %8zu\n";
    printf("%-30s : %8s %8s\n","field","offset","size");
    /*
(dolist (slot '(destination
                data
                bytesToSend
                complete
                completionProc
                completionRefCon
                ))
  (insert (format "printf(format,\"%s\",offsetof(struct MIDISysexSendRequest,%s),sizeof(r.%s));\n"
                  slot slot slot slot)))    
    */
    printf(format,"destination",offsetof(struct MIDISysexSendRequest,destination),sizeof(r.destination));
    printf(format,"data",offsetof(struct MIDISysexSendRequest,data),sizeof(r.data));
    printf(format,"bytesToSend",offsetof(struct MIDISysexSendRequest,bytesToSend),sizeof(r.bytesToSend));
    printf(format,"complete",offsetof(struct MIDISysexSendRequest,complete),sizeof(r.complete));
    printf(format,"completionProc",offsetof(struct MIDISysexSendRequest,completionProc),sizeof(r.completionProc));
    printf(format,"completionRefCon",offsetof(struct MIDISysexSendRequest,completionRefCon),sizeof(r.completionRefCon));

    printf("%-30s :          %8zu\n","sizeof(MIDISysexSendRequest)",sizeof(struct MIDISysexSendRequest));

    return 0;}
