#include <stdio.h>
#include <CoreMIDI/CoreMIDI.h>
// #define offsetof(s,field) (((char*)&(s.field))-((char*)&(s)))
int main(void){
    struct MIDISysexSendRequest r;
    /*
    (dolist (slot '(destination
    data
    bytesToSend
    complete
    completionProc
    completionRefCon
    )) 
    (insert (format "printf(\"%%-30s : %%lu %%lu\\n\",\"%s\",offsetof(struct MIDISysexSendRequest,%s),sizeof(r.%s));\n" 
    slot slot slot slot)))
    */
    
    printf("%-30s : %lu %lu\n","destination",offsetof(struct MIDISysexSendRequest,destination),sizeof(r.destination));
    printf("%-30s : %lu %lu\n","data",offsetof(struct MIDISysexSendRequest,data),sizeof(r.data));
    printf("%-30s : %lu %lu\n","bytesToSend",offsetof(struct MIDISysexSendRequest,bytesToSend),sizeof(r.bytesToSend));
    printf("%-30s : %lu %lu\n","complete",offsetof(struct MIDISysexSendRequest,complete),sizeof(r.complete));
    printf("%-30s : %lu %lu\n","completionProc",offsetof(struct MIDISysexSendRequest,completionProc),sizeof(r.completionProc));
    printf("%-30s : %lu %lu\n","completionRefCon",offsetof(struct MIDISysexSendRequest,completionRefCon),sizeof(r.completionRefCon));
    printf("%-30s : %lu\n","sizeof(MIDISysexSendRequest)",sizeof(struct MIDISysexSendRequest));
    
    return 0;}
