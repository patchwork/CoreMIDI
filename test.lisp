;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               test.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    Test the CoreMIDI system.
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2017-08-08 <PJB> Added this header.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;
;;;;    Copyright Pascal J. Bourguignon 2017 - 2017
;;;;
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************

(in-package "COM.INFORMATIMAGO.MACOSX.COREMIDI")
(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :com.informatimago.common-lisp.cesarum.simple-test)
  (use-package :com.informatimago.common-lisp.cesarum.utility)
  (use-package :com.informatimago.macosx.coremidi.midi))

(coremidi-framework)


(defun test-client-notify (message)
  (format t "MM: ~A~%" message)
  (force-output))


(defvar *effects* '())
(defun call-effects (&rest arguments)
  (mapc (lambda (effect)
          (handler-case (apply effect arguments)
            (error (err)
              (format t "~&EE: effect ~A error: ~A~%" effect err)
              (setf *effects* (delete effect *effects*)))))
        (copy-list *effects*))
  *effects*)

(defun make-echo-effect (port destination delay feedback)
  (check-type delay    (real 0.0))
  (check-type feedback (real 0.0 1.0))
  (when (< 0.9 feedback)
    (setf feedback 0.9))
  (let ((out-list '()))
    (lambda (message event refcon)
      (declare (ignore refcon))
      (ecase message
        ((:start-packet-list)
         (setf out-list '()))
        ((:end-packet-list)
         (when out-list
           (send port destination
                 (packet-list-from-messages (nreverse out-list)))
           (setf out-list '())))
        ((:message)
         (typecase event
           ((or midi:note-on-message midi:note-off-message)
            (loop
              :for velocity := (midi:message-velocity event)
                :then (truncate (* velocity feedback))
              :for i :from 1
              :for time := (+ (midi:message-time event)
                              (round (* i delay 1e9)))
              :do (push (print (make-instance (class-of event)
                                              :time     time
                                              :channel  (midi:message-channel event)
                                              :key      (midi:message-key     event)
                                              :velocity velocity))
                        out-list)
              :while (plusp velocity)))))))))


(defun line (x x0 y0 x1 y1)
  (assert (<= x0 x x1))
  (let ((slope (/ (- y1 y0) (- x1 x0))))
    (+ (* slope (- x x0)) y0)))

(defun envelope (time sustain-level a d s r)
  (check-type time (real 0.0))
  (cond
    ((< time a)              (max 0.1 (line time 0.0 0.0 a 1.0)))
    ((< (- time a)     d)    (line time a 1.0 (+ a d) sustain-level))
    ((< (- time a d)   s)    sustain-level)
    ((< (- time a d s) r)    (line time (+ a d s) sustain-level (+ a d s r) 0.0))
    (t                       0.0)))

#||
(loop
  :for time :from 0.0 :to 2.0 :by 0.01
  :for y := (envelope time 0.6 0.1 0.2 0.5 1.0)
  :do (format t "~10,2F ~10,2F~%" time y))
||#


(defun make-echo-effect/envelope (port destination delay feedback a d s r)
  (check-type delay    (real 0.0))
  (check-type feedback (real 0.0 1.0))
  (check-type a (real 0.0))             ; all times
  (check-type d (real 0.0))
  (check-type s (real 0.0)) ; sustain time; sustain value is feedback.
  (check-type r (real 0.0))
  (when (< 0.9 feedback)
    (setf feedback 0.9))
  (let ((out-list '()))
    (lambda (message event refcon)
      (declare (ignore refcon))
      (ecase message
        ((:start-packet-list)
         (setf out-list '()))
        ((:end-packet-list)
         (when out-list
           (send port destination
                 (packet-list-from-messages (nreverse out-list)))
           (setf out-list '())))
        ((:message)
         (typecase event
           ((or midi:note-on-message midi:note-off-message)
            (loop
              :for i :from 1
              :for dt := (* i delay)
              :for time := (+ (midi:message-time event) (round (* dt 1e9)))
              :for velocity := (truncate (* (midi:message-velocity event)
                                            (envelope dt feedback a d s r)))
              ;; :do (print `(:i ,i :dt ,dt :velocity ,velocity))
              :do (push (make-instance (class-of event)
                                       :time     time
                                       :channel  (midi:message-channel event)
                                       :key      (midi:message-key     event)
                                       :velocity velocity)
                        out-list)
              :while (plusp velocity)))))))))



(defun test-port-read (packet-list source-connection-refcon)
  ;;  (cffi:pointer-address source-connection-refcon)
  (let ((message-list (packet-list-to-messages packet-list)))
    (call-effects :start-packet-list message-list source-connection-refcon)
    (handler-case
        (dolist (message message-list)
          (unless (typep message 'midi:timing-clock-message)
            (format t "RR: ~A~%" message))
          (call-effects :message message source-connection-refcon))
      (error (err)
        (format t "RR: ~A~%" err)))
    (force-output)
    (call-effects :end-packet-list message-list source-connection-refcon)))


(defun test-destination-read (packet-list source-connection-refcon)
  (format t "DD: ~A <- ~A~%" packet-list source-connection-refcon)
  (force-output))

(defun collect-all-properties (object)
  (mapcar (lambda (property)
            (list (intern (string property) "KEYWORD")
                  (funcall property object)))
          (properties object)))

(defun set-equal (a b) (and (subsetp a b) (subsetp b a)))


(define-test test/client-create-dispose ()
  (let ((c1 (client-create "test-1" 'test-client-notify))
        (c2 (client-create "test-2" 'test-client-notify)))
    (assert (and (integerp (ref c1))
                 (integerp (ref c2))
                 (/= (ref c1)    (ref c2))))
    (assert (and (integerp (refcon c1))
                 (integerp (refcon c2))
                 (/= (refcon c1) (refcon c2))))
    (assert (eql (client-notify-function c1) 'test-client-notify))
    (assert (eql (client-notify-function c2) 'test-client-notify))
    (assert (eql (find-client-for-refcon (refcon c1)) c1))
    (assert (eql (find-client-for-refcon (refcon c2)) c2))
    (assert (member c1 (clients)))
    (assert (member c2 (clients)))
    (client-dispose c1)
    (assert (not (member c1 (clients))))
    (assert (member c2 (clients)))
    (client-dispose c2)
    (assert (not (member c1 (clients))))
    (assert (not (member c2 (clients))))))


(define-test test/devices ()
  (assert (eql (device-count) (length (devices))))
  (loop
    :with ds := (devices)
    :for i :below (device-count)
    :for d := (device-get i)
    :do (assert (member d ds))))



(define-test test/endpoints ()
  (assert (set-equal (union (sources) (destinations))
                     (endpoints))))




(define-test test/all ()
  (coremidi-framework)
  (test/client-create-dispose)
  (test/devices)
  (test/endpoints))





#||


(pprint (mapcar (lambda (device)
(mapcar (lambda (property)
(list (intern (string property) "KEYWORD")
(funcall property device)))
(properties 'device)))
(devices)))


(mapcar 'device-entities (devices))

(pprint (mapcar (lambda (device)
(mapcar (function name)
(cons device
(device-entities device))))
(devices)))

;; (("Bluetooth") ("IAC Driver" "Bus 1") ("Network" "Neptune") ("VMini" "Port 1" "Port 2") ("LPK25" "Akai LPK25 Wireless")
;;  ("VI61" "Port 1" "Port 2") ("PROZOR" "Port 1" "Port 2") ("Korg KRONOS" "SOUND" "KEYBOARD") ("UM-ONE" "UM-ONE")
;;  ("AKAI EWI5000" "EWI5000") ("Akai LPK25 Wireless" "Bluetooth"))



(setf *endpoints* nil)
(sources)
(destinations)




(pprint (mapcar (function collect-all-properties)
(endpoints)))

(pprint (mapcar (function collect-all-properties)
(devices)))

(pprint (mapcar (function collect-all-properties)
(external-devices)))


(intersection (sources) (destinations))

(mapcar 'list
(mapcar (compose name endpoint-entity) (endpoints))
(mapcar 'name (endpoints))
(mapcar 'model (endpoints)))


(mapcar (lambda (device)
(list (name device)
(mapcar (lambda (entity)
(list :name         (name entity)
:sources      (mapcar 'name (entity-sources entity))
:destinations (mapcar 'name (entity-destinations entity))))
(device-entities device))))
(external-devices))



*clients*
(mapc 'client-dispose *clients*)
(refcon *c*)
(ports)


(list (mapcar 'name (sources))
(mapcar (compose name  endpoint-entity) (sources))
(mapcar (compose name entity-device  endpoint-entity) (sources)))

(list (mapcar 'name (destinations))
(mapcar (compose name  endpoint-entity) (destinations))
(mapcar (compose name entity-device  endpoint-entity) (destinations)))

(mapcar (compose name entity-device endpoint-entity) (endpoints))

||#

(defvar *c*)
(defvar *po*)
(defvar *pi*)
(defvar *d*)

(defun test/setup ()
  (coreaudio-framework)
  (coremidi-framework)
  (defparameter *c*  (client-create "test" 'test-client-notify))
  (defparameter *po* (output-port-create *c* "test-out"))

  (defparameter *pi* (input-port-create  *c* "test-in" 'test-port-read))
  (port-connect-source *pi* (find-endpoint-named  "Korg KRONOS" "KEYBOARD" "KEYBOARD")
                       (cffi:make-pointer 42001))
  (port-connect-source *pi* (find-endpoint-named "VMini" "Port 1" "Out")
                       (cffi:make-pointer 42002))

  (defparameter *d* (find-endpoint-named  "Korg MS2000R" "Port 1" "Port 1")))
;;(defparameter *d* (destination-create *c* "test-destination" 'test-destination-read))



(defun test/send ()
  (let ((ti (current-host-time))
        (1s 1000000000)
        (channel 14))
    (flet ((in (n)
             (+ ti (* n 1s))))
      (send *po*  *d*
            (packet-list-from-messages
             (list  (make-instance 'midi::note-on-message :time (in 1) :status #x90  :channel channel :key 80 :velocity 70)
                    (make-instance 'midi::note-on-message :time (in 1) :status #x90  :channel channel :key 64 :velocity 70)
                    (make-instance 'midi::note-on-message :time (in 2) :status #x90  :channel channel :key 68 :velocity 40)
                    (make-instance 'midi::note-on-message :time (in 3) :status #x90  :channel channel :key 87 :velocity 80)
                    (make-instance 'midi::note-on-message :time (in 3) :status #x90  :channel channel :key 80 :velocity 80)
                    (make-instance 'midi::all-notes-off-message :time (in 5) :status #xb0)))))))





#||
(coreaudio-framework)
(coremidi-framework)
(test/send)
(push (make-echo-effect *po* *d* 0.300 0.45) *effects*)
(push (make-echo-effect/envelope *po* *d* 0.210  0.70  2.0 1.0 3.0 3.0) *effects*)
(pop *effects*)



(send *po*  *d*
(packet-list-from-lisp
(list (cons (+ 1000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key 100 :velocity 100)))
(cons (+ 1000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key  93 :velocity  90)))
(cons (+ 2000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key  88 :velocity  90)))
(cons (+ (* 3 1000000000) (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 0)))
(cons (+ (* 4 1000000000) (current-host-time))
(midi-event-to-buffer (make-instance 'midi::all-notes-off-message :status #xb0))))))

(send *po* *d*
(packet-list-from-lisp
(list (cons (current-host-time)
(midi-event-to-buffer (make-instance 'midi::all-notes-off-message :status #xb0))))))

(packet-list-to-lisp      (packet-list-from-lisp
(list (cons (+ 1000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key 100 :velocity 100)))
(cons (+ 1000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key  93 :velocity  90)))
(cons (+ 2000000000 (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-on-message  :status #x90 :channel 1 :key  88 :velocity  90)))
(cons (+ (* 3 1000000000) (current-host-time))
(midi-event-to-buffer (make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 0)))
(cons (+ (* 4 1000000000) (current-host-time))
(midi-event-to-buffer (make-instance 'midi::all-notes-off-message :status #xb0))))))


(send *po* *d* (packet-list-from-lisp (loop :for i from 20 to 100 by 5
:collect (cons 0 (vector #x90 i 65)))))


(packet-list-to-lisp (packet-list-from-lisp (loop :for i from 20 to 100 by 5
:collect (cons i (vector #x90 i 65)))))
(send *po* *d*
(packet-list-from-lisp (loop :for i from 20 to 100 by 5
:collect (cons i (vector #x90 i 65)))))
(cffi:foreign-alloc)
||#

#||

(cffi:defcstruct MIDIPacket
(timeStamp :unsigned-long-long)
(length :unsigned-short)
(data :pointer :count 256))

(cffi:defcstruct MIDIPacketList
(numPackets :unsigned-int)
(packet :pointer :count 1))

(cffi:defcstruct MIDISysexSendRequest
(destination :unsigned-int)
(data :pointer)
(bytesToSend :unsigned-int)
(complete :unsigned-char)
(reserved :pointer :count 3)
(completionProc :pointer)
(completionRefCon :pointer))

||#

(defun print-midi-devices ()
  (dolist (device (append (devices)
                          (external-devices)))
    (let ((entities      (device-entities device)))
      (format t "~30A ~%"
              (name device))
      (dolist (entity entities)
        (format t "          - ~A~@[ <- ~{~A~^, ~}~]~@[ -> ~{~A~^, ~}~]~%"
                (name entity)
                (mapcar (function name) (entity-sources entity))
                (mapcar (function name) (entity-destinations entity))))
      (terpri))))

;;(print-midi-devices)

#||



*po*
*pi*
*d*
(port-client *po*)
(port-client *pi*)
(endpoint-client *d*)
(port-sources *pi*)


(values (mapcar 'name (devices))
(mapcar 'name (external-devices)))
#|
("Bluetooth" "IAC Driver" "Network" "VMini" "LPK25" "VI61" "PROZOR" "Korg KRONOS" "UM-ONE" "AKAI EWI5000" "Akai LPK25 Wireless")
("neso" "sao" "Korg MS2000R" "Korg DSS-1" "Korg DW-8000" "Moog Subsequent 37 cv" "Schmidt Synthesizer" "AKAI EWI5000")
|#


#|
(("Bluetooth" nil) ("IAC Driver" ("Bus 1")) ("Network" ("Neptune")) ("VMini" ("Port 1" "Port 2"))
("LPK25" ("Akai LPK25 Wireless")) ("VI61" ("Port 1" "Port 2")) ("PROZOR" ("Port 1" "Port 2"))
("Korg KRONOS" ("SOUND" "KEYBOARD")) ("UM-ONE" ("UM-ONE")) ("AKAI EWI5000" ("EWI5000"))
("Akai LPK25 Wireless" ("Bluetooth")) ("neso" ("Port 1")) ("sao" ("Port 1")) ("Korg MS2000R" ("Port 1"))
("Korg DSS-1" ("Port 1")) ("Korg DW-8000" ("Port 1")) ("Moog Subsequent 37 cv" ("Port 1"))
("Schmidt Synthesizer" ("Port 1")) ("AKAI EWI5000" ("Port 1")))
|#


||#


#||
port
port-read-function
port-srcconnrefcon
port-client
input-port-create
output-port-create
port-connect-source
port-disconnect-source
port-dispose
ports
find-port-for-refcon



entity
entity-device
entity-destination
entity-destination-count
entity-destination-get
entity-destinations
entity-source
entity-source-count
entity-source-get
entity-sources

external-device
external-device-count
external-device-get
external-devices


||#

#||

(mapcar (function device-entities) (devices))
(defparameter *foob* (coremidi::client-create "FooB" (lambda (m) (format t "~&midi message ~S~%" m) (force-output))))
*foob*
*clients*
(coremidi::client-dispose *foob*)

||#

#||
(cffi:with-foreign-object (r '(:struct coremidi-low::MIDISysexSendRequest))

(setf (cffi:foreign-slot-value r '(:struct coremidi-low::MIDISysexSendRequest) 'coremidi-low::destination)
0))
||#


#||
(inspect (parse-midi  #(240 66 48 104 109 0 0 0 0 0 0 31 32 247)))

(midi:system-exclusive-message)

||#


#||



(midi:note-off-message)
(in-package :midi)
(make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 64)
(note-off-message)
(note-off-message)
(let ((self (make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 64)))
(print-parseable-object (self t :type t :identity nil) time status channel key velocity))
(note-off-message :time #:<unbound> :status 128 :channel 1 :key 100 :velocity 64)
(trace midi::call-print-parseable-object)
(midi::all-slots 'midi::note-off-message)
(make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 64)

(note-off-message)


(midi:note-off-message)
(time midi:status midi::channel midi::key midi::velocity)
(in-package :midi)
(pprint (macroexpand-1 '(define-midi-message note-off-message (voice-message)
:status-min #x80 :status-max #x8f
:slots ((key :initarg :key :reader message-key)
(velocity :initarg :velocity :reader message-velocity))
:filler (setf key next-byte
velocity next-byte)
:length 2
:writer (write-bytes key velocity))))

(progn (register-class 'note-off-message 'voice-message 128 143 nil nil)
(defclass note-off-message (voice-message)
((status-min :initform 128 :allocation :class) (status-max :initform 143 :allocation :class)
(data-min :initform nil :allocation :class) (data-max :initform nil :allocation :class)
(key :initarg :key :reader message-key) (velocity :initarg :velocity :reader message-velocity)))
(eval-when (:compile-toplevel :load-toplevel :execute)
(setf (gethash 'note-off-message *slots*) (list 'voice-message '(key velocity))))
(defmethod print-object ((self note-off-message) stream)
(print-parseable-object (self stream :type t :identity nil) time status channel key velocity))
(defmethod fill-message :after ((message note-off-message))
(with-slots (key velocity)
message
(symbol-macrolet ((next-byte (read-next-byte))) (setf key next-byte velocity next-byte))))
(defmethod length-message + ((message note-off-message))
(with-slots (status-min status-max data-min data-max key velocity) message 2))
(defmethod write-message :after ((message note-off-message))
(with-slots (status-min status-max data-min data-max key velocity) message (write-bytes key velocity))))


(midi-event-to-buffer (make-instance 'midi::note-off-message :status #x80 :channel 1 :key 100 :velocity 64))

||#


